# historical-flowmapper

### notes on JSON files
- There are 56 disinct chains of relations, stored in 56 different JSON files as edge lists (FP Chain #), and in a single concat. file (allchains.json)
    - Edge lists are between book IDs. 
- Book metadata is stored in books.json, which includes information such as publication location, date, and short title. 
- Temporal data is stored in timespans.json. 
    - In this file, each Book ID is assigned a timespan, which is considered as the time window in which the book exists. Links between books exist only when both books (of a single edge) are valid, i.e. intersection of their existence.  
- Geographic information is stored in places.json as long and lat. 